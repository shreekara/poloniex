//
//  SecondViewController.swift
//  Poloniex
//
//  Created by Shreekara on 1/27/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    @IBOutlet weak var accountNameLabel: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let savedUser = ArchiveStore.shared.getUser() {
            accountNameLabel.textLabel?.text = savedUser.hasLoggedIn ? savedUser.emailAddress : "Guest"
        } else {
            accountNameLabel.textLabel?.text = "Guest"
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

