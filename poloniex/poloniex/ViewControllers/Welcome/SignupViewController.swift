//
//  SignupViewController.swift
//  Poloniex
//
//  Created by Shreekara on 2/2/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    private enum Error {
        case firstNameEmpty
        case emailAddressEmpty
        case invalidEmailAddress
        case passwordEmpty
        case passwordMismatch
        case success
    }
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerUser(_ sender: UIButton) {
        if let firstNameStr = firstName.text, firstNameStr == "" {
            alertError(.firstNameEmpty)
            return
        }
        else if let emailAddressStr = emailAddress.text, emailAddressStr == "" {
            alertError(.emailAddressEmpty)
            return
        }
        else if let passwordStr = password.text, passwordStr == "" {
            alertError(.passwordEmpty)
            return
        }
        else if let passwordConfirmStr = confirmPassword.text, passwordConfirmStr == "" {
            alertError(.passwordEmpty)
            return
        }
        if !emailAddress.text!.isValidEmail() {
            alertError(.invalidEmailAddress)
            return
        }
        if password.text! != confirmPassword.text! {
            alertError(.passwordMismatch)
            return
        }
        let user = User(firstName: firstName.text!,
                             lastName: lastName.text ?? "",
                             emailAddress: emailAddress.text!,
                             password: confirmPassword.text!,
                             hasLoggedIn: false)
        if ArchiveStore.shared.saveUser(user) {
            alertError(.success)
        }
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    private func alertError(_ error: Error) {
        switch error {
        case .firstNameEmpty:
            let alert = UIAlertController(title: "Error", message: "First name cannot be empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .emailAddressEmpty:
            let alert = UIAlertController(title: "Error", message: "Email cannot be empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .invalidEmailAddress:
            let alert = UIAlertController(title: "Error", message: "Invalid email address", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .passwordEmpty:
            let alert = UIAlertController(title: "Error", message: "Password fields cannot be empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .passwordMismatch:
            let alert = UIAlertController(title: "Error", message: "Password mismatch", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .success:
            let alert = UIAlertController(title: "Success", message: "Account created. Please sign in now.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            present(alert, animated: true, completion: nil)
        }
    }
}

extension String {
    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
