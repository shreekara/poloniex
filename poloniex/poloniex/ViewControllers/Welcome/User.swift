//
//  User.swift
//  Poloniex
//
//  Created by Shreekara on 2/2/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import Foundation

class User {
    let firstName: String
    let lastName: String
    let emailAddress: String
    let password: String
    var hasLoggedIn: Bool

    init(_ info: NSDictionary) {
        firstName = info["firstName"] as? String ?? ""
        lastName = info["lastName"] as? String ?? ""
        emailAddress = info["emailAddress"] as? String ?? ""
        password = info["password"] as? String ?? ""
        hasLoggedIn = info["hasLoggedIn"] as? Bool ?? false
    }
    
    init(firstName: String, lastName: String, emailAddress: String, password: String, hasLoggedIn: Bool) {
        self.firstName = firstName
        self.lastName = lastName
        self.emailAddress = emailAddress
        self.password = password
        self.hasLoggedIn = hasLoggedIn
    }
    
    func info() -> NSDictionary {
        let infoDict = NSMutableDictionary.init()
        infoDict.setObject(firstName, forKey: "firstName" as NSCopying)
        infoDict.setObject(lastName, forKey: "lastName" as NSCopying)
        infoDict.setObject(emailAddress, forKey: "emailAddress" as NSCopying)
        infoDict.setObject(password, forKey: "password" as NSCopying)
        infoDict.setObject(hasLoggedIn, forKey: "hasLoggedIn" as NSCopying)
        return infoDict
    }
}
