//
//  WelcomeViewController.swift
//  Poloniex
//
//  Created by Shreekara on 2/2/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    private enum Error {
        case emailEmpty
        case invalidEmailAddress
        case passwordEmpty
        case loginFailure
    }

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.text = ""
        passwordField.text = ""
        if ArchiveStore.shared.getUser()?.hasLoggedIn ?? false {
            if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController() {
                navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    //MARK:- Actions
    @IBAction func loginAsGuest(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController() {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        if let emailAddressStr = emailField.text, emailAddressStr == "" {
            alertError(.emailEmpty)
            return
        }
        else if let emailAddressStr = emailField.text, !emailAddressStr.isValidEmail() {
            alertError(.invalidEmailAddress)
            return
        }
        else if let passwordStr = passwordField.text, passwordStr == "" {
            alertError(.passwordEmpty)
            return
        }
        
        if let savedUser = ArchiveStore.shared.getUser() {
            if savedUser.password == passwordField.text! && savedUser.emailAddress == emailField.text! {
                savedUser.hasLoggedIn = true
                let saveStatus = ArchiveStore.shared.saveUser(savedUser)
                guard saveStatus == true else {
                    alertError(.loginFailure)
                    return
                }
                if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController() {
                    navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                alertError(.loginFailure)
            }
        } else {
            alertError(.loginFailure)
        }
    }
    
    private func alertError(_ error: Error) {
        switch error {
        case .emailEmpty:
            let alert = UIAlertController(title: "Error", message: "Email cannot be empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .passwordEmpty:
            let alert = UIAlertController(title: "Error", message: "Password cannot be empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .invalidEmailAddress:
            let alert = UIAlertController(title: "Error", message: "Invalid email address", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .loginFailure:
            let alert = UIAlertController(title: "Error", message: "Login failed ", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
}
