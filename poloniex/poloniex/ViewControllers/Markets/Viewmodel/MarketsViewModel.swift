//
//  MarketsViewModel.swift
//  Poloniex
//
//  Created by Shreekara on 1/29/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import Foundation

enum ViewMode {
    case list
    case grid
}

enum ColumnHeaderOption {
    case BTC
    case USDC
    case ETH
    case XMR
    case USDT
    
    func getTitle() -> String {
        switch self {
        case .BTC:
            return "BTC"
        case .USDC:
            return "USDC"
        case .ETH:
            return "ETH"
        case .XMR:
            return "XMR"
        case .USDT:
            return "USDT"
        }
    }
    
    func getColumnHeaderOption(for index: Int) -> ColumnHeaderOption {
        switch index {
        case 0:
            return .BTC
        case 1:
            return .USDC
        case 2:
            return .ETH
        case 4:
            return .XMR
        case 5:
            return .USDT
        default:
            return .BTC
        }
    }
}

enum ColumnFields {
    case COIN
    case VOLUME
    case PRICE
    case TWENTYFOUR_HOUR
    case LOWEST_ASK
    case HIGHEST_BID
    case INTRADAY_HIGHEST
    case INTRADAY_LOWEST
    
    func getTitle() -> String {
        switch self {
        case .COIN:
            return "Coin"
        case .VOLUME:
            return "Volume"
        case .PRICE:
            return "Price"
        case .TWENTYFOUR_HOUR:
            return "24h"
        case .LOWEST_ASK:
            return "Lowest Ask"
        case .HIGHEST_BID:
            return "Highest Bid"
        case .INTRADAY_HIGHEST:
            return "Intra High"
        case .INTRADAY_LOWEST:
            return "Intra Low"
        }
    }
    
    func fieldValueFromTicker(_ tickerInfo: PoloniexTickerInfo, viewMode: ViewMode) -> String {
        switch self {
        case .COIN:
            return viewMode == .grid ? tickerInfo.secondCurrency : tickerInfo.currencyPairName ?? ""
        case .VOLUME:
            if let baseCurrencyVolumeDouble = Double(tickerInfo.baseCurrencyVolume) {
                return String(format: "%0.3f", baseCurrencyVolumeDouble)
            }
            return tickerInfo.baseCurrencyVolume
        case .PRICE:
            return tickerInfo.lastTradePrice
        case .TWENTYFOUR_HOUR:
            if let last24ChangeDouble = Double(tickerInfo.last24Change) {
                if last24ChangeDouble == 0 {
                    return "+0.00 %"
                }
                return String(format: "%0.2f %@", last24ChangeDouble*100, "%")
            }
            return "+0.00 %"
        case .LOWEST_ASK:
            return tickerInfo.lowestAsk
        case .HIGHEST_BID:
            return tickerInfo.highestBid
        case .INTRADAY_HIGHEST:
            return tickerInfo.last24HighestTradePrice
        case .INTRADAY_LOWEST:
            return tickerInfo.last24LowestTradePrice
        }
    }
    
    func rawFieldValueFromTicker(_ tickerInfo: PoloniexTickerInfo, viewMode: ViewMode) -> String {
        switch self {
        case .COIN:
            return viewMode == .grid ? tickerInfo.secondCurrency : tickerInfo.currencyPairName ?? ""
        case .VOLUME:
            return tickerInfo.baseCurrencyVolume
        case .PRICE:
            return tickerInfo.lastTradePrice
        case .TWENTYFOUR_HOUR:
            return tickerInfo.last24Change
        case .LOWEST_ASK:
            return tickerInfo.lowestAsk
        case .HIGHEST_BID:
            return tickerInfo.highestBid
        case .INTRADAY_HIGHEST:
            return tickerInfo.last24HighestTradePrice
        case .INTRADAY_LOWEST:
            return tickerInfo.last24LowestTradePrice
        }
    }
}

protocol MarketsViewModelDelegate {
    func reloadData()
    func reloadItemAt(_ index: Int)
}

class MarketsViewModel {
    
    var displayMode: ViewMode = .grid
    var delegate: MarketsViewModelDelegate?
    private weak var updatedService: PoloniexService? {
        didSet {
            if oldValue == nil {
                reloadTickerLists()
            }
        }
    }
    var selectedColumn: ColumnHeaderOption = .BTC
    var selectedTickerField: ColumnFields = .COIN
    private var columnHeaderOptions: [ColumnHeaderOption] = [.BTC, .USDC, .ETH, .XMR, .USDT]
    private (set) var columnTickerFieldOptions: [ColumnFields] = [.COIN, .VOLUME, .PRICE, .TWENTYFOUR_HOUR]
    private (set) var sortAscending: Bool = true
    private var processedTickerLists: [PoloniexTickerInfo] = []

    init() {
        BitcoinServiceManager.shared.registerObserver(self)
    }
    
    deinit {
        BitcoinServiceManager.shared.removeObserver(self)
    }
    
    //MARK:- Sorting & Processing
    
    private func performSorting() {
        processedTickerLists = processedTickerLists.sorted(by: { left, right in
            let leftValue = selectedTickerField.rawFieldValueFromTicker(left, viewMode: displayMode)
            let rightValue = selectedTickerField.rawFieldValueFromTicker(right, viewMode: displayMode)
            if selectedTickerField == .COIN {
                return (sortAscending ? leftValue < rightValue : leftValue >= rightValue)
            } else if let leftDoubleValue = Double(selectedTickerField.rawFieldValueFromTicker(left, viewMode: displayMode)),
                let rightDoubleValue = Double(selectedTickerField.rawFieldValueFromTicker(right, viewMode: displayMode)) {
                return (sortAscending ? leftDoubleValue < rightDoubleValue : leftDoubleValue >= rightDoubleValue)
            }
            return false
        })
    }
    
    private func processTickerUpdate(_ updatedTicker: PoloniexTickerInfo) {
        var needsSorting = true
        var updatedRow = -1
        var existingTickerInfo: PoloniexTickerInfo?
        
        if displayMode == .grid &&
           selectedColumn.getTitle() == updatedTicker.firstCurrency {
            if let index = processedTickerLists.firstIndex(of: updatedTicker) {
                needsSorting = !(selectedTickerField.fieldValueFromTicker(processedTickerLists[index], viewMode: displayMode) == selectedTickerField.fieldValueFromTicker(updatedTicker, viewMode: displayMode))
                existingTickerInfo = processedTickerLists[index]
                processedTickerLists[index] = updatedTicker
                updatedRow = index
            } else {
                processedTickerLists.append(updatedTicker)
            }
        } else if displayMode == .list {
            if let index = processedTickerLists.firstIndex(of: updatedTicker) {
                needsSorting = !(selectedTickerField.fieldValueFromTicker(processedTickerLists[index], viewMode: displayMode) == selectedTickerField.fieldValueFromTicker(updatedTicker, viewMode: displayMode))
                existingTickerInfo = processedTickerLists[index]
                processedTickerLists[index] = updatedTicker
                updatedRow = index
            } else {
                processedTickerLists.append(updatedTicker)
            }
        }
        if needsSorting {
            performSorting()
        }
        if let oldValue = existingTickerInfo {
            for field in columnTickerFieldOptions {
                if field.fieldValueFromTicker(oldValue, viewMode: displayMode) != field.fieldValueFromTicker(updatedTicker, viewMode: displayMode) {
                    delegate?.reloadItemAt(updatedRow)
                    break
                }
            }
        } else {
            delegate?.reloadData()
        }
    }
    
    private func reloadTickerLists() {
        processedTickerLists.removeAll()
        if displayMode == .grid,
           let tickerDict = updatedService?.tickerMapTable[selectedColumn.getTitle()]?.values {
            for tickerInfo in tickerDict {
                processedTickerLists.append(tickerInfo)
            }
        } else {
            if let tickerList = updatedService?.tickerListTable.values {
                for tickerInfo in tickerList {
                    processedTickerLists.append(tickerInfo)
                }
            }
        }
    }
    
    //MARK:- Actions
    func toggleViewMode() {
        let toggledMode:ViewMode = displayMode == .grid ? .list : .grid
        displayMode = toggledMode
        reloadTickerLists()
        delegate?.reloadData()
    }
    
    func selectSortOptionAt(_ index: Int) {
        if columnTickerFieldOptions[index] != selectedTickerField {
            selectedTickerField = columnTickerFieldOptions[index]
        } else {
            sortAscending = !sortAscending
        }
        performSorting()
        delegate?.reloadData()
    }
    
    func selectHeaderOptionAt(_ index: Int) {
        if selectedColumn != columnHeaderOptions[index] {
            selectedColumn = columnHeaderOptions[index]
            reloadTickerLists()
            delegate?.reloadData()
        }
    }

    func selectFieldOptionAt(_ index: Int) {
        if selectedTickerField != columnTickerFieldOptions[index] {
            selectedTickerField = columnTickerFieldOptions[index]
            reloadTickerLists()
            delegate?.reloadData()
        }
    }

    //MARK:- Column info
    func columnHeaderCount() -> Int {
        return displayMode == .grid ? columnHeaderOptions.count : 0
    }
    
    func columnHeaderTitle(at index: Int) -> String {
        return displayMode == .grid ? columnHeaderOptions[index].getTitle() : ""
    }
    
    func tickerFieldCount() -> Int {
        return columnTickerFieldOptions.count
    }
    
    func tickerFieldTitle(at index: Int) -> String {
        return columnTickerFieldOptions[index].getTitle()
    }
    
    //MARK:- Row info
    func numberOfRows() -> Int {
        return processedTickerLists.count
    }
    
    func numberOfItemsAtIndex(_ index: Int) -> Int {
        return tickerFieldCount()
    }
    
    func fieldValueForItem(at row: Int, column: Int) -> String {
        let columnField = columnTickerFieldOptions[column]
        let tickerInfo = processedTickerLists[row]
        return columnField.fieldValueFromTicker(tickerInfo, viewMode: displayMode)
    }
}

extension MarketsViewModel: BitcoinServiceDelegate {
    var uid: String {
        return "MarketsViewModel"
    }
    
    func serviceDidConnect(_ service: BitcoinService) {
        
    }
    
    func serviceDidDisconnect(_ service: BitcoinService) {
        
    }
    
    func serviceDidReceiveMessage(_ service: BitcoinService, updatedTickerInfo: TickerInfo?) {
        if let poloniexService = service as? PoloniexService {
            updatedService = poloniexService
            if let updatedTicker = updatedTickerInfo as? PoloniexTickerInfo {
                processTickerUpdate(updatedTicker)
            }
        }
    }
}
