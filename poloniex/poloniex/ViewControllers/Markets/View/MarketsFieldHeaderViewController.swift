//
//  MarketsFieldHeaderViewController.swift
//  Poloniex
//
//  Created by Shreekara on 2/2/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import UIKit

protocol MarketsFieldHeaderViewDelegate: class {
    func optionDidTap(_ sender: UIButton)
    func titleTextAt(_ index: Int) -> String
}

class MarketsFieldHeaderViewController: UIViewController {

    weak var delegate: MarketsFieldHeaderViewDelegate?
    @IBOutlet weak var optionsView: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        updateButtonTitles()
    }
    
    func updateButtonTitles() {
        for button in optionsView.arrangedSubviews {
            if let tabButton = button as? UIButton {
                tabButton.setTitle(delegate?.titleTextAt(tabButton.tag), for: .normal)
                tabButton.setTitle(delegate?.titleTextAt(tabButton.tag), for: .highlighted)
            }
        }
    }
    
    @IBAction func optionDidTap(_ sender: UIButton) {
        delegate?.optionDidTap(sender)
        updateButtonTitles()
    }
}
