//
//  FirstViewController.swift
//  Poloniex
//
//  Created by Shreekara on 1/27/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import UIKit

class MarketsViewController: UIViewController {
    //Constants
    private enum Constants {
        static let cellIdentifier = "marketCell"
        static let fieldHeaderIdentifier = "fieldHeader"
        static let cellHeight: CGFloat = 44.0
    }
    
    //Outlets
    @IBOutlet private weak var segmentControlHolderViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var viewModeButton: UIBarButtonItem!
    @IBOutlet private weak var columnSegmentControl: UISegmentedControl!
    @IBOutlet private weak var marketsTableView: UITableView!
    
    //Viewmodel
    private var viewModel = MarketsViewModel()
    
    private var filterPrice: String?

    //MARK:- Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        columnSegmentControl.setDividerImage(UIImage(named: "alphaGradient"),
                                             forLeftSegmentState: .normal,
                                             rightSegmentState: .normal,
                                             barMetrics: .default)
        columnSegmentControl.setDividerImage(UIImage(named: "alphaGradient"),
                                             forLeftSegmentState: .highlighted,
                                             rightSegmentState: .highlighted,
                                             barMetrics: .default)
        columnSegmentControl.setDividerImage(UIImage(named: "alphaGradient"),
                                             forLeftSegmentState: .selected,
                                             rightSegmentState: .selected,
                                             barMetrics: .default)
        viewModeButton.image = viewModel.displayMode == .grid ? UIImage(named: "grid") : UIImage(named: "list")
        viewModel.delegate = self
        let searchBarframe = CGRect(x: 0,
                                    y: 0,
                                    width: marketsTableView.frame.size.width,
                                    height: Constants.cellHeight)
        let searchDisplayController = UISearchBar(frame: searchBarframe)
        searchDisplayController.searchBarStyle = .minimal
        searchDisplayController.tintColor = .black
        searchDisplayController.delegate = self
        searchDisplayController.placeholder = "Price highlight"
        marketsTableView.tableHeaderView = searchDisplayController
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MarketsFieldHeaderViewController {
            vc.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        marketsTableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Actions
    @IBAction func toggleViewMode(_ sender: UIBarButtonItem) {
        viewModel.toggleViewMode()
        if viewModel.displayMode == .grid {
            sender.image = UIImage(named: "grid")
            segmentControlHolderViewHeight.constant = Constants.cellHeight
        } else {
            sender.image = UIImage(named: "list")
            segmentControlHolderViewHeight.constant = 0
        }
    }
    
    @IBAction func headerOptionDidChange(_ sender: UISegmentedControl) {
        viewModel.selectHeaderOptionAt(sender.selectedSegmentIndex)
    }
    
    //MARK:- Private functions
    private func dequeueReusableCell(for indexPath: IndexPath) -> MarketsTableViewCell? {
        let cellID = Constants.cellIdentifier + String(indexPath.row)
        var dequeuedCell: MarketsTableViewCell?
        if let cell = marketsTableView.dequeueReusableCell(withIdentifier: cellID) as? MarketsTableViewCell {
            dequeuedCell = cell
        } else {
            marketsTableView.register(UINib(nibName: "MarketsTableViewCell",
                                            bundle: nil),
                                      forCellReuseIdentifier: cellID)
            dequeuedCell = marketsTableView.dequeueReusableCell(withIdentifier: cellID) as? MarketsTableViewCell
        }
        return dequeuedCell
    }
}

extension MarketsViewController: UITableViewDataSource, UITableViewDelegate {
    //MARK:- Table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(for: indexPath)
        for i in 0..<viewModel.numberOfItemsAtIndex(indexPath.row) {
            var fieldValue = viewModel.fieldValueForItem(at: indexPath.row, column: i)
            var textColor: UIColor = .black
            if viewModel.columnTickerFieldOptions[i] == .TWENTYFOUR_HOUR {
                textColor = fieldValue.hasPrefix("-") ? UIColor.redTickerFieldColor() : UIColor.greenTickerFieldColor()
            } else if viewModel.columnTickerFieldOptions[i] == .PRICE {
                if let filterPriceStr = filterPrice,
                    let searchedPrice = Double(filterPriceStr),
                    let priceDouble = Double(fieldValue) {
                    fieldValue = (priceDouble < searchedPrice) ? String(format: "▼ %@", fieldValue) : String(format: "▲ %@", fieldValue)
                    textColor = (priceDouble < searchedPrice) ?  UIColor.redTickerFieldColor() : UIColor.greenTickerFieldColor()
                }
            }
            cell?.updateText(fieldValue, at: i, textColor: textColor)
        }
        return cell!
    }
}

extension MarketsViewController: MarketsViewModelDelegate {
    //MARK:- Ticker refresh
    func reloadItemAt(_ index: Int) {
        let reloadedIndexPath = IndexPath(row: index, section: 0)
        let refreshCell = dequeueReusableCell(for: reloadedIndexPath)
        var textColor: UIColor = .black
        for i in 0..<viewModel.numberOfItemsAtIndex(index) {
            var fieldValue = viewModel.fieldValueForItem(at: index, column: i)
            if viewModel.columnTickerFieldOptions[i] == .TWENTYFOUR_HOUR {
                textColor = fieldValue.hasPrefix("-") ?  UIColor.redTickerFieldColor() : UIColor.greenTickerFieldColor()
            } else if viewModel.columnTickerFieldOptions[i] == .PRICE {
                if let filterPriceStr = filterPrice,
                   let searchedPrice = Double(filterPriceStr),
                   let priceDouble = Double(fieldValue) {
                    fieldValue = (priceDouble < searchedPrice) ? String(format: "▼ %@", fieldValue) : String(format: "▲ %@", fieldValue)
                    textColor = (priceDouble < searchedPrice) ?  UIColor.redTickerFieldColor() : UIColor.greenTickerFieldColor()
                }
            }
            refreshCell?.updateText(fieldValue, at: i, textColor: textColor)
        }
    }
    
    func reloadData() {
        marketsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellHeight
    }
}

extension MarketsViewController: MarketsFieldHeaderViewDelegate {
    //MARK:- Field sorting
    func titleTextAt(_ index: Int) -> String {
        var title = viewModel.tickerFieldTitle(at: index)
        if title == viewModel.selectedTickerField.getTitle() {
            title = viewModel.sortAscending ? String(format: "%@ ▼", title) : String(format: "%@ ▲", title)
        }
        return title
    }
    
    func optionDidTap(_ sender: UIButton) {
        viewModel.selectSortOptionAt(sender.tag)
    }
}

extension MarketsViewController: UISearchBarDelegate {
    //MARK:- Search handling
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterPrice = searchText
        reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
