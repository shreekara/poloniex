//
//  MarketsTableViewCell.swift
//  Poloniex
//
//  Created by Shreekara on 1/29/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import UIKit

class MarketsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelsHolderStackView: UIStackView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateText(_ text: String, at index: Int, textColor: UIColor) {
        labelAt(index)?.text = text
        labelAt(index)?.textColor = textColor
    }
    
    func labelAt(_ index: Int) -> UILabel? {
        return labelsHolderStackView.arrangedSubviews[index] as? UILabel
    }
}
