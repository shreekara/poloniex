//
//  ArchiveStore.swift
//  Poloniex
//
//  Created by Shreekara on 2/3/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import Foundation

class ArchiveStore {
    
    static let shared = ArchiveStore()
    private var userStorePath: String?
    
    init() {
        userStorePath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        userStorePath = (userStorePath as NSString?)?.appendingPathComponent("user.plist")
    }
    
    func saveUser(_ user: User) -> Bool {
        if let userPath = userStorePath {
            if FileManager.default.fileExists(atPath: userPath) {
                try? FileManager.default.removeItem(atPath: userPath)
            }
            return user.info().write(toFile: userPath, atomically: true)
        }
        return false
    }
    
    func getUser() -> User? {
        if let userPath = userStorePath {
            if FileManager.default.fileExists(atPath: userPath) {
                if let info = NSDictionary.init(contentsOfFile: userPath) {
                    return User(info)
                }
            }
        }
        return nil
    }
}
