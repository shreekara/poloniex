//
//  BitcoinServiceManager.swift
//  Poloniex
//
//  Created by Shreekara on 1/28/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import Foundation
import Starscream

protocol BitcoinService {
    var socketURL: URL? { get }
    var subscriptionMessage: String { get }
    @discardableResult func parseMessage(_ message: String) -> TickerInfo?
}

protocol BitcoinServiceDelegate {
    var uid: String { get }
    func serviceDidConnect(_ service: BitcoinService)
    func serviceDidDisconnect(_ service: BitcoinService)
    func serviceDidReceiveMessage(_ service: BitcoinService, updatedTickerInfo: TickerInfo?)
}

protocol TickerInfo {
    var currencyPairName: String? { get }
    var firstCurrency: String { get }
    var secondCurrency: String { get }
}

class BitcoinServiceManager {
    private var registeredServices: [String: BitcoinService] = [:]
    private var connectionPool: [String: WebSocket] = [:]
    private var observers: [BitcoinServiceDelegate] = []
    static let shared = BitcoinServiceManager()
    
    init() {
        registerService(PoloniexService())
    }
    
    deinit {
        observers.removeAll()
    }
    
    //MARK:- Actions
    func registerService(_ service: BitcoinService) {
        if let socketURL = service.socketURL {
            registeredServices[socketURL.absoluteString] = service
            connectionPool[socketURL.absoluteString] = WebSocket(url: socketURL)
        }
    }
    
    func registerObserver(_ observer: BitcoinServiceDelegate) {
        observers.append(observer)
    }

    func removeObserver(_ observer: BitcoinServiceDelegate) {
        if let index = observers.firstIndex(where: { return $0.uid == observer.uid } ) {
            observers.remove(at: index)
        }
    }

    func connect(_ service: BitcoinService?  = nil) {
        if let url = service?.socketURL {
            connectionPool[url.absoluteString]?.delegate = self
            connectionPool[url.absoluteString]?.connect()
        } else {
            for socket in connectionPool.values {
                socket.delegate = self
                socket.connect()
            }
        }
    }
    
    func disconnect(_ service: BitcoinService? = nil) {
        if let socketKey = service?.socketURL?.absoluteString {
            connectionPool[socketKey]?.disconnect()
        } else {
            for socket in connectionPool.values {
                socket.delegate = self
                socket.disconnect()
            }
        }
    }
}

extension BitcoinServiceManager: WebSocketDelegate {
    //MARK:- WebSocketDelegates
    func websocketDidConnect(socket: WebSocketClient) {
        if let webSocket: WebSocket = socket as? WebSocket,
           let service = registeredServices[webSocket.currentURL.absoluteString] {
            webSocket.write(string: service.subscriptionMessage)
            _ = observers.compactMap( { $0.serviceDidConnect(service) })
        }
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        if let webSocket: WebSocket = socket as? WebSocket,
            let service = registeredServices[webSocket.currentURL.absoluteString] {
            webSocket.write(string: service.subscriptionMessage)
            _ = observers.compactMap( { $0.serviceDidDisconnect(service) })
        }
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        if let webSocket: WebSocket = socket as? WebSocket,
            let service = registeredServices[webSocket.currentURL.absoluteString] {
            let updatedInfo = service.parseMessage(text)
            _ = observers.compactMap( { $0.serviceDidReceiveMessage(service, updatedTickerInfo: updatedInfo) })
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        
    }
}
