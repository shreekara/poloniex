//
//  PoloniexTickerInfo.swift
//  Poloniex
//
//  Created by Shreekara on 1/28/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import Foundation
import UIKit

/*
 [ <id>, null, [ <currency pair id>, "<last trade price>", "<lowest ask>", "<highest bid>", "<percent change in last 24 hours>", "<base currency volume in last 24 hours>", "<quote currency volume in last 24 hours>", <is frozen>, "<highest trade price in last 24 hours>", "<lowest trade price in last 24 hours>" ], ... ]
 
 < [1002,1]
 < [1002,null,[149,"219.42870877","219.85995997","219.00000016","0.01830508","1617829.38863451","7334.31837942",0,"224.44803729","214.87902002"]]
 < [1002,null,[150,"0.00000098","0.00000099","0.00000098","0.01030927","23.24910068","23685243.40788439",0,"0.00000100","0.00000096"]]
 < [1002,null,[162,"0.00627869","0.00630521","0.00627608","0.01665689","17.99294312","2849.74975814",0,"0.00640264","0.00615185"]]
 */

struct PoloniexTickerInfo: TickerInfo {
    let id: Int
    let currencyPairID: Int
    let lastTradePrice: String
    let lowestAsk: String
    let highestBid: String
    let last24Change: String
    let baseCurrencyVolume: String
    let quoteCurrencyVolume: String
    let isFrozen: Bool
    let last24HighestTradePrice: String
    let last24LowestTradePrice: String
    var currencyPairName: String?
    var firstCurrency: String {
        let name =  currencyPairName?.split(separator: "/").first ?? ""
        return String(name)
    }
    var secondCurrency: String {
        let name =  currencyPairName?.split(separator: "/").last ?? ""
        return String(name)
    }
    
    func hasValidCurrencyPairInfo() -> Bool {
        return currencyPairName != nil
    }
    
    func description() {
        print("id                       : \(id)")
        print("currencyPairID / name    : \(currencyPairID) / \(currencyPairName ?? "unknown")")
        print("lastTradePrice           : \(lastTradePrice)")
        print("lowestAsk                : \(lowestAsk)")
        print("highestBid               : \(highestBid)")
        print("last24Change             : \(last24Change)")
        print("baseCurrencyVolume       : \(baseCurrencyVolume)")
        print("quoteCurrencyVolume      : \(quoteCurrencyVolume)")
        print("isFrozen                 : \(isFrozen)")
        print("last24HighestTradePrice  : \(last24HighestTradePrice)")
        print("last24LowestTradePrice   : \(last24LowestTradePrice)")
        print("----------------------------------------------------------------")
    }
}

//MARK:- Parsing
extension PoloniexTickerInfo: Decodable {
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        id = try container.decode(Int.self)
        _ = try container.decodeNil()
        var tickerInfo = try container.nestedUnkeyedContainer()
        currencyPairID = try tickerInfo.decode(Int.self)
        lastTradePrice = try tickerInfo.decode(String.self)
        lowestAsk = try tickerInfo.decode(String.self)
        highestBid = try tickerInfo.decode(String.self)
        last24Change = try tickerInfo.decode(String.self)
        baseCurrencyVolume = try tickerInfo.decode(String.self)
        quoteCurrencyVolume = try tickerInfo.decode(String.self)
        isFrozen = try tickerInfo.decode(Int.self) != 0
        last24HighestTradePrice = try tickerInfo.decode(String.self)
        last24LowestTradePrice = try tickerInfo.decode(String.self)
        if let pairName = Utilities.currencyPair(for: currencyPairID) {
            currencyPairName = pairName
        } else {
            currencyPairName = Utilities.currencyName(for: currencyPairID)
        }
    }
}

//MARK:- Equatable
extension PoloniexTickerInfo: Equatable {
    public static func == (lhs: PoloniexTickerInfo, rhs: PoloniexTickerInfo) -> Bool {
        return lhs.currencyPairID == rhs.currencyPairID
    }
}
