//
//  PoloniexService.swift
//  Poloniex
//
//  Created by Shreekara on 1/28/19.
//  Copyright © 2019 BitOasis. All rights reserved.
//

import Foundation

class PoloniexService: BitcoinService {
    var tickerMapTable: [String: [String: PoloniexTickerInfo]] = [:]
    var tickerListTable: [String: PoloniexTickerInfo] = [:]
    var socketURL: URL?  {
        return URL(string: "wss://api2.poloniex.com")
    }
    
    var subscriptionMessage: String {
        return "{\"command\":\"subscribe\",\"channel\":\"1002\"}"
    }
    
    func parseMessage(_ message: String) -> TickerInfo? {
        var info: TickerInfo?
        if let jsonData = message.data(using: .utf8),
            let tickerInfo = try? JSONDecoder().decode(PoloniexTickerInfo.self, from: jsonData),
            tickerInfo.hasValidCurrencyPairInfo() {
            info = tickerInfo
            //tickerInfo.description()
            if var tickerMap =  tickerMapTable[tickerInfo.firstCurrency] {
                tickerMap[tickerInfo.secondCurrency] = tickerInfo
                tickerMapTable[tickerInfo.firstCurrency] = tickerMap
            } else {
                tickerMapTable[tickerInfo.firstCurrency] = [tickerInfo.secondCurrency: tickerInfo]
            }
            tickerListTable[tickerInfo.currencyPairName ?? ""] = tickerInfo
        }
        return info
    }
}
